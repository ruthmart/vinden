<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name', 'legal_name', 'legal_representative', 'phone',
        'email', 'rfc', 'logo', 'status', 'is_active'
    ];

    protected $hidden = [ 'created_at', 'created_by', 'updated_at', 'created_by' ];
}

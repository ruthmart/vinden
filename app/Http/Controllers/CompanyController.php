<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Collective\Html\FormFacade;
use Collective\Html\HtmlFacade;
use Illuminate\Support\Facades\DB;
use App\Company as Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('company.index-company');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('company.create-company', [ 'company' => new Company ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $datosEmpresa = new Company;
        // $datosEmpresa->create($request->all());
        $datosEmpresa->name = $request->name;
        $datosEmpresa->legal_name = $request->legal_name;
        $datosEmpresa->legal_representative = $request->legal_representative;
        $datosEmpresa->phone = $request->phone;
        $datosEmpresa->email = $request->email;
        $datosEmpresa->rfc = $request->rfc;
        $datosEmpresa->logo = '';
        $datosEmpresa->status = 'Activo';
        $datosEmpresa->is_active = 1;
        $datosEmpresa->save();

        // $datosEmpresa = $request->all();
        // $datosEmpresa=request()->except('_token');

        // if($request->hasFile('logo')){
        //     $datosEmpresa['logo']=$request->file('logo')->store('uploads','public');
        // }
        
        
        // return response()->json($datosEmpresa);


        return redirect()->action('CompanyController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        return view('company.create-company', [ 'company' => $company ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datosEmpresa = Company::findOrFail($id);
        // $datosEmpresa->create($request->all());
        $datosEmpresa->name = $request->name;
        $datosEmpresa->legal_name = $request->legal_name;
        $datosEmpresa->legal_representative = $request->legal_representative;
        $datosEmpresa->phone = $request->phone;
        $datosEmpresa->email = $request->email;
        $datosEmpresa->rfc = $request->rfc;
        $datosEmpresa->save();

        return redirect()->action('CompanyController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Company::destroy($id);
        // return redirect('companies');
        return response ('exito', 200);
    }

    public function all(){
        $companies = Company::all();
        return response()->json($companies);
    }
}


@extends('layouts.master')

@section('title', 'Empresas')

@section('styles')
@endsection

@section('content')
@include('layouts.tabs')
<div class="container">
    <div class="table-responsive-md">
        <index-company>
        </index-company>
    </div>
</div>
@endsection

@section('scripts')
<!-- <script src="{{ asset('js/company.js') }}"></script> -->
@endsection

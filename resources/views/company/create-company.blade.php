@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<br>
@if(Request::is('companies/create'))
    <h5>Crear empresa </h5>
@elseif(Request::is('companies/*'))
    <h5>Editar empresa: '{{ $company->name }}'</h5>
@endif
<br>
<div class="container">
    <div class="col-lg-10 col-12">
        <div class="row">
            <div class="col-10 col-12">
                <form action="{{ Request::is('companies/create') ? url('/companies') : url('/companies/' . $company->id)}}" method="POST" enctype="multipart/form-data">
                @if(Request::is('companies/create'))
                    {{ method_field('POST') }}
                @elseif(Request::is('companies/*'))
                    {{ method_field('PATCH') }}
                @endif
                {{ csrf_field() }}
                <div class="form-group1">
                    <label for="name" style="text-align: left;">{{'Nombre:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1 v-form-input" type="text" name="name" required="required" data-error="Obligatorio" id="name" value="{{ $company->name }}">
                    </div> 
                </div>         
                <div class="form-group1">              
                    <label for="legal_name">{{'Razón Social:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1" type="text" name="legal_name" required="required" data-error="Obligatorio" id="legal_name" value="{{ $company->legal_name }}">
                    </div> 
                </div>   
                <div class="form-group1"> 
                    <label for="legal_representative">{{'Representante Legal:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1" type="text" name="legal_representative"required="required" data-error="Obligatorio" id="legal_representative" value="{{ $company->legal_representative }}">
                    </div> 
                </div>
                <div class="form-group1"> 
                    <label for="phone">{{'Télefono:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1" type="text" maxlength="12" name="phone" id="phone" value="{{ $company->phone }}">
                    </div> 
                </div>
                <div class="form-group1">
                    <label for="email">{{'Correo:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1" type="text" name="email" id="email" value="{{ $company->email }}">
                    </div> 
                </div>
                <div class="form-group1">               
                    <label for="rfc">{{'RFC:'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="form-control1 " type="text" name="rfc" id="rfc" value="{{ $company->rfc }}"required="required" data-error="Obligatorio" >
                    </div> 
                </div>
                <div class="form-group1">
                    <label class="tex-label" for="logo">{{'Logo'}}</label>
                    <div class="col-lg-4 col-12">
                        <input class="fc" type="file" name="logo" id="logo" value="{{ $company->logo }}">
                    </div> 
                </div>    
            <br>
            <br>
         <input class ="button" type="submit" value="Agregar">

                </form>
            
        </div>
    </div>
</div>
@endsection

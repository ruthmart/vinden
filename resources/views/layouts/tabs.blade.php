<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vindenn - @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    @yield('styles')
</head>
<body>
<div class="container">
        <br>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active opensans" data-toggle="tab" href="/companies">Empresa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#negocio">Unidad de Negocio</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#vehiculo">Vehículo</a>
        </li>
    </ul>
     <!-- Tab panes -->
  <div class="tab-content">
    <main id="app" class="content container tab-pane active">
        @yield('content')
    </main>

  </div>
</body>
</html>